<launch>

    <arg name="vehicle" default="sentry"/>
    <arg name="actuator_ns" default="/$(arg vehicle)/actuators"/>
    <arg name="reference_ns" default="/$(arg vehicle)/references"/>
    <arg name="dynamics_ns" default="/$(arg vehicle)/dynamics"/>
    <arg name="nav_ns" default="/$(arg vehicle)/nav"/>
    <arg name="sensors_ns" default="/$(arg vehicle)/sensors"/>

    <arg name="active_controller" default="2"/>

    <arg name="nav_state_topic" default="/$(arg nav_ns)/agg/state"/>

    <env name="ROSCONSOLE_CONFIG_FILE" value="$(find sentry_config)/config/rosconsole.config"/>

    <include file="$(find sentry_config)/machine/$(env ROBOT).machine" />
    <!-- URDF -->
    <param name="robot_description" textfile="$(find sentry_config)/vehicle_model/sentry_sim.urdf"/>

    <group ns="sentry">
        <node name="robot_state_publisher" pkg="robot_state_publisher" type="robot_state_publisher"/>
        <node name="sentry_abort_supervisor" pkg="abort_supervisor" type="sentry_abort_supervisor"/>

        <!-- Dynamics -->
        <include file="$(find sentry_config)/config/dynamics/launch.xml"/>

        <!-- Controllers -->
        <include file="$(find sentry_config)/config/controllers/launch.xml">
            <arg name="active_controller" value="$(arg active_controller)"/>
        </include>

        <!-- Devices -->
        <include file="$(find sentry_config)/config/devices/launch.xml" />

        <!-- Nav -->
        <param name="sim/enable_tf_broadcast" value="false" type="bool"/>
        <param name="sim/tf_frame_name" value="sim_frame" type="str"/>
        <param name="sim/enable_navstate_broadcast" value="false" type="bool"/>
        <include file="$(find sentry_config)/config/nav/launch.xml" />

        <!-- Goals -->
        <include file="$(find sentry_config)/config/goals/launch.xml"/>

        <!--
        <group ns="controllers">
            <param name="active_controller" value="1" type="int"/>
            <param name="actuator_ns" value="/sentry/actuators" type="string"/>
            <param name="reference_ns" value="/sentry/references" type="string"/>
            <param name="controller_ns" value="/sentry/controllers" type="string"/>

            <node name="actuator_fanout" pkg="sentry_control" type="actuator_fanout"/>

            <node name="surface" pkg="sentry_control" type="controller" args="surface">
                <param name="references/joystick" value="rf_joystick"/>
                <param name="state_input_topic" value="/sentry/nav/agg/state"/>
            </node>
            <node name="idle" pkg="sentry_control" type="controller" args="idle">
                <param name="state_input_topic" value="/sentry/nav/agg/state"/>
            </node>
        </group>

        <group ns="references">
            <param name="active_reference" value="1" type="int"/>
            <node name="rf_joystick" pkg="sentry_control" type="reference" args="rf" output="screen">
                <param name="input/velocity_u" value="LEFT_STICK_VERT" type="string"/>
                <param name="input/rotation_rate_w" value="LEFT_STICK_HORZ" type="string"/>
                <param name="input/velocity_w" value="RIGHT_STICK_VERT" type="string"/>
                <param name="joystick_topic" value="/sentry/devices/rf_joystick/joy" type="string"/>
            </node>
        </group>

        <group ns="devices">
            <remap from="joy" to="rf_joystick/joy"/>
            <node name="rf_joystick" pkg="joy" type="joy_node">
                <param name="autorepeat_rate" value="5" type="double"/>
            </node>
        </group>
        -->

        <!--
        <group ns="actuators">
            <node name="thruster_aft_port" pkg="ds_elmo" type="sentry_thruster">
               <param name="name" value="thruster_aft_port" type="string"/>
               <param name="command_topic" value="thruster_aft_port" type="string"/>
               <param name="elmo/type" value="SERIAL" type="string"/>
               <param name="elmo/port" value="/dev/ttyUSB0" type="string"/>
               <param name="elmo/baud" value="19200" type="int"/>
               <param name="elmo/matcher" value="match_char" type="string"/>
               <param name="elmo/delimiter" value="3B" type="string"/>
            </node>
        </group>
        -->
    </group>

    <!-- Gazebo! -->
    <include file="$(find gazebo_ros)/launch/empty_world.launch">
      <arg name="world_name" value="$(find ds_sim)/world/underwater_empty.world"/>
      <arg name="paused" value="false"/>
      <arg name="use_sim_time" value="true"/>
      <arg name="gui" value="false"/>
      <arg name="recording" value="false"/>
      <arg name="debug" value="true"/>
      <arg name="verbose" value="true"/>
      <arg name="extra_gazebo_args" value="-s libdsros_sensors.so"/>
    </include>


    <node name="spawn_urdf" pkg="gazebo_ros" type="spawn_model"
          args="-file $(find sentry_config)/vehicle_model/sentry_sim.urdf -urdf -x 10.0 -y 5 -z -100 -Y 1.5 -model sentry"/>
    <node name="rviz" pkg="rviz" type="rviz" args="-d $(find sentry_config)/rviz/sentry_sim.rviz"/>

</launch>




