#!/bin/bash

# Get the directory this script is located in
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

MY_IP="$(hostname -I | grep -oP "(192\.168\.100\.[0-9]+)")"

# Load the standard ROS stuff
source $DIR/setup.bash

# Set my own IP
export ROS_IP=$MY_IP

# Start my own master
export ROS_MASTER_URI=http://192.168.100.100:11311/
