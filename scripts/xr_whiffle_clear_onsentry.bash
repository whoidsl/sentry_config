#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

source $DIR/sentry_setup.bash

ROBOTNAME=sentry

set -e

echo $"Clearing XR1"
rosservice call /$ROBOTNAME/hotel/grd/xr1/set_code "code: 0"
echo $"Clearing XR2"
rosservice call /$ROBOTNAME/hotel/grd/xr2/set_code "code: 0"
