#!/usr/bin/env python

import argparse
import glob
import os.path

# BASH has no good sorting implementation, so we're going to use
# python to get the most recent

parser = argparse.ArgumentParser(description='Link the origin for the highest-numbered dive to the place where ROS will look for it')
parser.add_argument('source')
parser.add_argument('destination')
args = parser.parse_args()

globstring = 'sentry*_ros_org.yaml'
files = sorted(glob.glob(os.path.join(args.source, globstring)), reverse=True)

if len(files) < 1:
    print("Didn't find any files matching the string '"+globstring+"'. Is there a mission available?")
    print("Could not load mission, and so will not be able to link origin. Abort!")
    raise Exception("could not load mission; could not link origin")

source = os.path.abspath(files[0])
dest   = os.path.abspath(args.destination)
print('+-----------------------------------------------------------+')
print('| Linking origin file:                                      |')
print('|   %-50s      |' % source)
print('| to:                                                       |')
print('|   %-50s      |' % dest)
print('+-----------------------------------------------------------+')
cmd = 'ln -sf %s %s' % (source, dest)
print("preparing to execute \n\"%s\"" % cmd)
ret = os.system(cmd)
if ret != 0:
  raise Exception("Unable to link origin!")

