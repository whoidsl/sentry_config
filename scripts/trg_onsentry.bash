#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

source $DIR/sentry_setup.bash

ROBOTNAME=sentry
SERVICE=sonars/trigger/board_enablement

set -e
shopt -s nocasematch

case "$1" in
	on)
		echo $"Enabling triggers"
		rosservice call /$ROBOTNAME/$SERVICE "data: true"
		;;
	off)
		echo $"Disabling triggers"
		rosservice call /$ROBOTNAME/$SERVICE "data: false"
		;;
	*)
		echo -e $"Usage: $0 {on|off}\n\nEnable/Disable Sentry's trigger board."
		exit 1
esac
