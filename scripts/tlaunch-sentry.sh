#!/bin/bash

# First command line argument specifies active window at startup

tmux new-session -s sentry -d -P -n hotel   -c "$HOME/sentry_ws" "bash --login && source $HOME/sentry_ws/devel/sentry_setup.bash"
tmux set-option -t sentry set-remain-on-exit on
tmux set-option -t sentry history-limit 2000
tmux set-option -t sentry mouse on
tmux new-window -t sentry:1 -n vehicle -c "$HOME/sentry_ws" "bash --login && source $HOME/sentry_ws/devel/sentry_setup.bash"
tmux new-window -t sentry:2 -n mc      -c "$HOME/git/mc/src" "bash --login"
tmux new-window -t sentry:3 -n kongsberg -c "$HOME" "bash --login"
tmux new-window -t sentry:4 -n testing -c "$HOME" "bash --login"

# Split windows in panes at startup is p is specified
if test "$1" = "p"; then
    tmux join-pane -v -s vehicle -t hotel
    tmux join-pane -v -s mc -t hotel
    tmux join-pane -v -s kongsberg -t hotel
    tmux join-pane -v -s testing -t hotel
    tmux select-layout tiled
    tmux -2 attach-session -t sentry
fi

# Key bindings
tmux bind-key P join-pane -s vehicle -t hotel \\\; join-pane -s mc -t hotel \\\; select-layout tile
tmux bind-key L break-pane -s vehicle -t vehicle \\\; break-pane -s mc -t mc
tmux bind-key R respawn-window
tmux bind-key A respawn-window -k
if test "$2" = "c"; then
    tmux select-window -t sentry:$1
    tmux -2 attach-session -t sentry
fi
