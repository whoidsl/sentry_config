#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

source $DIR/sentry_setup.bash

# Index servos
echo "Index foreward servo..."
rosservice call /sentry/actuators/servo_fwd/ctrl_cmd 1

echo "Index aft servo..."
rosservice call /sentry/actuators/servo_aft/ctrl_cmd 1

echo "Done indexing."

exit
