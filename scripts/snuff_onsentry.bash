#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

source $DIR/sentry_setup.bash

# Snuff Burnwires
echo "Snuff port..."
rosservice call /sentry/hotel/grd/xr2/cmd 4

echo "Snuff descent and starboard..."
rosservice call /sentry/hotel/grd/xr1/cmd 4

echo "Done snuffing."

exit
