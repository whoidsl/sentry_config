#!/usr/bin/env python

#from ds_param import *
import ds_param

class CmdLeg(object):
    """Implements a command to control the controller type"""

    def __init__(self):
        self._start_easting  = None
        self._start_northing = None
        self._end_easting    = None
        self._end_northing   = None


    def parse_args(self, args):
        """Parse all arguments.  Either throw an execption, or be ready-to-run"""
        if len(args) < 4:
            raise ValueError('Not enough coordinates specified!')

        self._start_easting  = float(args[0])
        self._start_northing = float(args[1])
        self._end_easting    = float(args[2])
        self._end_northing   = float(args[3])

    def execute(self):
        """Actually execute the commands.  Args is a list of string arguments from argparse"""

        print('Setting sentry''s leg goal to (%12.1f E,%12.1f N) --> (%12.1f E,%12.1f N)' % (self._start_easting,
                                                                                             self._start_northing,
                                                                                             self._end_easting,
                                                                                             self._end_northing))

        conn = ds_param.ParamConnection()

        p_start_easting  = conn.connect('/sentry/goals/leg/start_easting' , ds_param.DoubleParam, False)
        p_start_northing = conn.connect('/sentry/goals/leg/start_northing', ds_param.DoubleParam, False)
        p_end_easting    = conn.connect('/sentry/goals/leg/end_easting'   , ds_param.DoubleParam, False)
        p_end_northing   = conn.connect('/sentry/goals/leg/end_northing'  , ds_param.DoubleParam, False)

        conn.lock()

        try:
            p_start_easting .set(self._start_easting)
            p_start_northing.set(self._start_northing)

            p_end_easting .set(self._end_easting)
            p_end_northing.set(self._end_northing)
            conn.unlock()

        except:
            conn.unlock()
            raise #rethrow!


    @staticmethod
    def help():
        print("""
        
        Set the current trackline.  The command is:
        
        sentry_cmd leg [start easting] [start northing] [end easting] [end northing]
        
        All positions are easting/northing relative in meters relative to the origin.
        The line runs from the start to the end (of course!)
        """)

    @staticmethod
    def args_help():
        return '[start easting] [start northing] [end easting] [end northing]'

# Expose this class to the wider world
CmdName = 'LEG'
CmdClass = CmdLeg